 # QuasiGO Compiler <img src="https://golang.org/doc/gopher/gopherbw.png" alt="Gopher" width="75" height="75"> 

Java implementation of the Go compiler. The compiler may be logically split in four phases:

1. **Parsing**
	1. tokenize source code (lexical analysis) ✔️
	2. parse source code (syntax analysis) ✔️
	3. syntax tree construction for each source file ✔️

2. **Type-checking and AST transformations**
	1. create compiler AST ✳️
	2. type checking ✳️
	3. AST transformations ❌

3. **Generic Static Single Assignment (SSA)**
	1. converting to SSA ❌
	2. SSA passes and rules ❌

4. **Generating machine code**
	1. SSA lowering and arch-specific passes ❌
	2. machine code generation ❌

* [The Go Programming Language Specification](https://golang.org/ref/spec)
* [Introduction to the Go compiler](https://github.com/golang/go/blob/master/src/cmd/compile/README.md)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Running the tests

Explain how to run the automated tests for this system ...

## Built With

* [Eclipse IDE](https://www.eclipse.org/) - Java IDE used
* [Maven](https://maven.apache.org/) - Dependency Management
* [JFlex](http://jflex.de/) - Lexical analyzer generator used
* [CUP](http://www2.cs.tum.edu/projekte/cup/index.php) -  LALR parser generator for Java used
* [CUP Eclipse Plugin](http://www2.cs.tum.edu/projekte/cup/eclipse.php) -  IDE plugin for developing CUP based parsers

## Contributing

Please read [CONTRIBUTING.md](https://github.com/golf-sierra/lexical-analyzer/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Felipe Matheus** - [felipemcs](https://github.com/felipemcs)
* **Gabriel Soares** - [golf-sierra](https://github.com/golf-sierra)
* **Katharine Padilha** - [katharinepadilha](https://github.com/katharinepadilha)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspiration
* ...
