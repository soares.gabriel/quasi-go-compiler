package pasito;

import pasito.ast.*;
import pasito.ast.PasitoVisitor;
import pasito.ast.SourceFile;
import pasito.ast.declaration.*;
import pasito.ast.element.*;
import pasito.ast.expression.*;
import pasito.ast.importDecl.Declaration;
import pasito.ast.methodSpecOrInterfaceName.*;
import pasito.ast.signature.*;
import pasito.ast.statement.*;
import pasito.ast.topLevelDecl.*;
import pasito.ast.type.*;

import java.awt.image.PixelInterleavedSampleModel;
import java.nio.channels.Pipe;
import java.util.Collections;
import java.util.List;

/**
 * Created by ariel on 27/08/17.
 */
public class PrettyPrint implements PasitoVisitor {

	private int indentLevel = 0;
	private StringBuilder result = new StringBuilder();

	public PrettyPrint (){}
	public PrettyPrint(SourceFile sourcefile) {
		System.out.println(this.VisitSourceFile(sourcefile));
	}

	private void indent() { ++indentLevel; }
	private void unindent() { --indentLevel; }

	/**
	 * Mtodo auxiliar para impresso de listas de strings
	 * @param list - lista de strings
	 * @param parentheses - vai haver parenteses?
	 * @return
	 */
	private String printList(List<?> list, boolean parentheses) {
		StringBuilder sb = new StringBuilder();
		sb.append(list.get(0));
		for (Object item : list.subList(1, list.size())) sb.append(", " + item);
		if (parentheses) {sb.insert(0, '('); sb.append(')');}
		return sb.toString();
	}

	/**
	 * Mtodo auxiliar para impresso de strings
	 * @param str - string
	 * @param indent - identao?
	 * @return
	 */
	private Object print (Object str, boolean indent) {
		String tabs = indent && indentLevel > 0 ?
				String.join("", Collections.nCopies(indentLevel, "   ")) : "";
				return tabs + str;
	}


	@Override
	public Object VisitSourceFile(SourceFile sourceFile) {
		StringBuilder result = new StringBuilder();
		for (TopLevelDecl declaration: sourceFile.topLevelDeclarations) {
			result.append(print(declaration.accept(this) + ";", true));
		}
		return result.toString();
	}

	@Override
	public Object VisitDeclaration(Declaration declaration) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Object VisitDec(Dec dec) {
		return dec.decl.accept(this);
	}

	@Override
	public Object VisitFunctionDecl(FunctionDecl functionDecl) {
		StringBuilder sb = new StringBuilder();

		// @TO-DO: CONFERIR!!
		sb.append("func " + functionDecl.name + " ");
		sb.append(functionDecl.sig.accept(this));
		sb.append(functionDecl.body.accept(this));
		return sb.toString();
	}

	@Override
	public Object VisitMethodDecl(MethodDecl methodDecl) {

		StringBuilder sb = new StringBuilder();
		sb.append(methodDecl.receiver + " ");
		sb.append("method" + methodDecl.name + " ");
		sb.append(methodDecl.sig.accept(this));
		sb.append(methodDecl.body.accept(this));

		return sb.toString();
	}

	@Override
	public Object VisitSignature(Signature signature) {

		try {
			if(!signature.outParameters.isEmpty()) {
				return printList(signature.outParameters, true);	
			}else if(!signature.inParameters.isEmpty()){
				return printList(signature.inParameters, true);
			}else if(!signature.variadicParameter.equals(null)){
				return print(signature.variadicParameter, false);
			}else {
				return print("( )",false);
			}
		}
		catch(NullPointerException n) {
			return print("( )",false);
		}

	}

	@Override
	public Object VisitConstDecl(ConstDecl constDecl) {
		StringBuilder sb = new StringBuilder();

		sb.append(constDecl.name+" "); 
		sb.append(constDecl.type.accept(this)+" "); 
		sb.append(constDecl.exp.accept(this));

		return sb;
	}

	@Override
	public Object VisitVarDecl(VarDecl varDecl) {
		StringBuilder sb = new StringBuilder();
		sb.append("var ");
		sb.append(varDecl.name + " ");
		sb.append(varDecl.type.accept(this) + " ");
		if(varDecl.exp != null)
			sb.append(" = ");
			sb.append(varDecl.exp.accept(this));
		return sb;
	}

	@Override
	public Object VisitTypeDecl(TypeDecl typeDecl) {

		StringBuilder sb = new StringBuilder();
		sb.append(typeDecl.name + " ");
		sb.append(typeDecl.type.accept(this) + " ");

		return sb;
	}

	@Override
	public Object VisitFormalParameter(FormalParameter formalParameter) {
		StringBuilder sb = new StringBuilder();
		sb.append(formalParameter.name + " ");
		sb.append(formalParameter.type.accept(this) + " ");
		return sb;
	}

	@Override
	public Object VisitTypeName(TypeName typeName) {
		return typeName.accept(this);
	}

	@Override
	public Object VisitArrayType(ArrayType arrayType) {
		StringBuilder sb = new StringBuilder();
		sb.append("Array [");
		sb.append(arrayType.length.accept(this));
		sb.append("] of ");
		sb.append(arrayType.elemType.accept(this));
		return sb;
	}

	@Override
	public Object VisitBaseType(PointerType pointerType) {
		return print(pointerType.accept(this), false);
	}

	@Override
	public Object VisitStructType(StructType structType) {

		return printList(structType.fieldDecls, false);
		// verificar com o professor

	}

	@Override
	public Object VisitInterfaceType(InterfaceType interfaceType) {
		return printList(interfaceType.interfaceElems, false);
	}

	@Override
	public Object VisitSliceType(SliceType sliceType) {
		return print(sliceType.elemType.accept(this),false);
	}

	@Override
	public Object VisitFieldDecl(FieldDecl fieldDecl) {
		StringBuilder sb = new StringBuilder();
		sb.append(fieldDecl.name + " ");
		sb.append(fieldDecl.type.accept(this));
		return sb;

	}

	@Override
	public Object VisitMethodSpec(MethodSpec methodSpec) {
		StringBuilder sb = new StringBuilder();
		sb.append(methodSpec.name + " ");
		sb.append(methodSpec.sig.accept(this));
		return sb;
	}

	@Override
	public Object VisitInterfaceName(InterfaceName interfaceName) {
		return print(interfaceName.name,false);
	}

	@Override
	public Object VisitUnaryExpression(UnaryExpression unaryExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(unaryExpression.op+" ");
		sb.append(unaryExpression.exp.accept(this));
		return sb;
	}

	@Override
	public Object VisitBinaryExpression(BinaryExpression binaryExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(binaryExpression.leftExp.accept(this));
		sb.append(binaryExpression.op);
		sb.append(binaryExpression.rightExp.accept(this));
		return sb;
	}

	@Override
	public Object VisitIntLiteral(IntLiteral intLiteral) {
		return intLiteral.value;
		// tem que retornar literal ou string ?
	}

	@Override
	public Object VisitFloatLiteral(FloatLiteral floatLiteral) {
		return floatLiteral.value;
	}

	@Override
	public Object VisitFunctionLiteral(FunctionLiteral functionLiteral) {
		StringBuilder sb = new StringBuilder();
		sb.append(functionLiteral.sig.accept(this));
		sb.append(functionLiteral.body.accept(this));
		return sb;
	}

	@Override
	public Object VisitCompositLit(CompositeLit compositeLit) {
		StringBuilder sb = new StringBuilder();
		sb.append(compositeLit.type.accept(this)+" ");
		sb.append(printList(compositeLit.elems,false));
		return sb;
	}

	@Override
	public Object VisitIdExpression(IdExpression idExpression) {
		return idExpression.name;
	}

	@Override
	public Object VisitMethodExpression(MethodExpression methodExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(methodExpression.type.accept(this)+" ");
		sb.append(methodExpression.name);
		return sb;
	}

	@Override
	public Object VisitSelectorExpression(SelectorExpression selectorExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(selectorExpression.exp.accept(this)+" ");
		sb.append(selectorExpression.name);
		return sb;

	}

	@Override
	public Object VisitIndexExpression(IndexExpression indexExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(indexExpression.exp.accept(this)+" ");
		sb.append(indexExpression.indexExp.accept(this));
		return sb;
	}

	@Override
	public Object VisitSliceExpression(SliceExpression sliceExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(sliceExpression.exp.accept(this)+" ");
		sb.append(sliceExpression.low.accept(this)+" ");
		sb.append(sliceExpression.high.accept(this));
		return sb;
	}

	@Override
	public Object VisitFullSliceExpression(FullSliceExpression fullSliceExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(fullSliceExpression.exp.accept(this)+" ");
		sb.append(fullSliceExpression.low.accept(this)+" ");
		sb.append(fullSliceExpression.high.accept(this)+" ");
		sb.append(fullSliceExpression.max.accept(this));
		return sb;
	}

	@Override
	public Object VisitCallExpression(CallExpression callExpression) {
		StringBuilder sb = new StringBuilder();
		sb.append(".");
		sb.append(callExpression.exp.accept(this));
		sb.append("(");

//		if() {
//
//
//			for (Expression exp : callExpression.args) {
//				int tam = callExpression.args.size() -1;
//				
//			}
//			
//		}

		sb.append(")");

		return null;
	}

	@Override
	public Object VisitKeyedElement(KeyedElement keyedElement) {
		StringBuilder sb = new StringBuilder();
		sb.append(keyedElement.elem.accept(this));
		sb.append(" : ");
		sb.append(keyedElement.exp.accept(this));
		return sb.toString();
	}

	@Override
	public Object VisitExpressionElement(ExpressionElement expressionElement) {
		StringBuilder sb = new StringBuilder();
		sb.append(expressionElement.exp.accept(this));	
		return sb.toString();
	}

	@Override
	public Object VisitLiteralElement(LiteralElement literalElement) {
		StringBuilder sb = new StringBuilder();
		for (KeyedElement key : literalElement.keyedElems) {
			sb.append(key.accept(this));
		}
		return sb.toString();
	}

	@Override
	public Object VisitDeclarationStm(DeclarationStm declarationStm) {
		StringBuilder sb = new StringBuilder();
		sb.append(declarationStm.decl.accept(this));
		return sb.toString();
	}

	@Override
	public Object VisitEmptyStmt(EmptyStmt emptyStmt) {
		return null;
	}

	@Override
	public Object VisitReturnStmt(ReturnStmt returnStmt) {
		StringBuilder sb = new StringBuilder();
		sb.append("return ");
		for(Expression exp : returnStmt.exps) {
			sb.append(exp.accept(this));
		}
		return sb.toString();
	}

	@Override
	public Object VisitExpressionStmt(ExpressionStmt expressionStmt) {
		StringBuilder sb = new StringBuilder();
		sb.append(expressionStmt.exp.accept(this));
		return sb.toString();

	}

	@Override
	public Object VisitAssignment(Assignment assignment) {
		StringBuilder sb = new StringBuilder();
		int sizeLeft = assignment.leftExps.size()-1;
		int sizeRight = assignment.rightExps.size()-1;

		// Pega todas as expresses da esquerda
		for (Expression exp : assignment.leftExps.subList(0, sizeLeft))
			sb.append((String) exp.accept(this) + ',');
		sb.append(assignment.leftExps.get(sizeLeft));

		// adiciona sinal =
		sb.append(" = ");

		// Pega todas as expresses da direita
		for (Expression exp : assignment.rightExps.subList(0, sizeRight))
			sb.append((String) exp.accept(this) + ',');
		sb.append(assignment.rightExps.get(sizeRight));

		return sb.toString();
	}

	@Override
	public Object VisitShortVarDecl(ShortVarDecl shortVarDecl) {
		StringBuilder sb = new StringBuilder();
		int sizeLeft = shortVarDecl.names.size()-1;
		int sizeRight = shortVarDecl.names.size()-1;

		// Pega todas as expresses da esquerda
		for (String name : shortVarDecl.names.subList(0, sizeLeft)) {
			sb.append(name  + ',');
		}

		sb.append(shortVarDecl.names.get(sizeLeft));

		// adiciona sinal =
		sb.append(" = ");

		// Pega todas as expresses da direita
		for (String name : shortVarDecl.names.subList(0, sizeRight)) {
			sb.append( name + ',');
		}
		sb.append(shortVarDecl.names.get(sizeRight));

		return sb.toString();
	}

	@Override
	public Object VisitBlock(Block block) {
		StringBuilder result = new StringBuilder();

		indent();
		for (Statement stm : block.stmts) {
			if (stm instanceof EmptyStmt)
				continue; // 
			result.append(stm.accept(this)+";");
		}
		unindent();

		return result.toString();
	}

	@Override
	public Object VisitIfStmt(IfStmt ifStmt) {
		StringBuilder result = new StringBuilder();

		result.append("if ");
		if (ifStmt.initStmt != null)
			result.append(ifStmt.initStmt.accept(this) + " ");
		result.append(ifStmt.exp.accept(this));
		indent();
		result.append(ifStmt.block.accept(this));
		unindent();

		return result.toString();
	}

	@Override
	public Object VisitIfElseStmt(IfElseStmt ifElseStmt) {
		StringBuilder result = new StringBuilder();

		result.append("if ");
		if (ifElseStmt.initStmt != null)
			result.append(ifElseStmt.initStmt.accept(this) + " ");
		result.append(ifElseStmt.exp.accept(this));
		indent();
		result.append(ifElseStmt.leftBlock.accept(this));
		unindent();
		result.append("else ");
		if(ifElseStmt.rightBlock !=null)
			indent();
		result.append(ifElseStmt.rightBlock.accept(this)); 
		unindent();
		return result.toString();
	}

	@Override
	public Object VisitForStmt(ForStmt forStmt) {
		StringBuilder result = new StringBuilder();
		result.append("for ");
		if(forStmt.initStmt!=null) {


			result.append(forStmt.initStmt.accept(this));
			result.append(";");
			result.append(forStmt.exp.accept(this));
			result.append(";");
			result.append(forStmt.postStmt.accept(this));
			result.append(".");
			indent();
			result.append(forStmt.body.accept(this));
			unindent();}else
			{
				result.append(forStmt.exp.accept(this));
				indent();
				result.append(forStmt.body.accept(this));
				unindent();

			}

		return result.toString();
	}

	@Override
	public Object VisitForRange(ForRange forRange) {
		StringBuilder result = new StringBuilder();
		result.append("for ");
		if(forRange.exp!=null) {
			//result.append(forRange.exp.accept(this));
			result.append("range");
			result.append(forRange.rangExp.accept(this));
			result.append(".");
			indent();
			result.append(forRange.body.accept(this));
			unindent();} 	
		return result.toString();
	}

	@Override
	public Object VisitBinaryOperator(BinaryOperator binaryOperator) {
		switch (binaryOperator.name()) {
		case "AND": return print(") AND (", false);
		case "OR": return print(") OR (", false);
		case "PLUS": return print(" + ", false);
		case "MINUS": return print(" - ", false);
		case "TIMES": return print(" * ", false);
		case "DIV": return print(" / ", false);
		case "LESS": return print(" < ", false);
		case "EQUAL": return print(" = ", false);
		default: return null;
		}
	}

	@Override
	public Object VisitUnaryOperator(UnaryOperator unaryOperator) {
		switch (unaryOperator.name()) {
		case "PLUS": return print(" + ", false);
		case "MINUS": return print(" - ", false);
		case "NOT": return print(" ~ ", false);
		case "POINTED_BY": return print(" * ", false);
		default: return null;
		}
	}
	@Override
	public Object VisitPrimitiveType(PrimitiveType primitiveType) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitBooleanConstant(BooleanConstant booleanConstant) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitVariadicCommaOp(VariadicCommaOp variadicCommaOp) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Object VisitMapType(MapType mapType) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	public Object VisitChannelType(ChannelType channelType) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitStringLiteral(StringLiteral stringLiteral) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitFunctionType(FunctionType functionType) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitAliasDecl(AliasDecl aliasDecl) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitTypeNameQualified(TypeNameQualified typeNameQualified) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitTag(Tag tag) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitRuneLiteral(RuneLiteral runeLiteral) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object VisitImaginaryLiteral(ImaginaryLiteral imaginaryLiteral) {
		// TODO Auto-generated method stub
		return null;
	}
}
