package pasito.ast.importSpec;

import pasito.ast.PasitoVisitor;
import pasito.ast.importDecl.Declaration;

import java.nio.file.Paths;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Gabriel Soares on 09/03/18.
*/
public class ImportSpec extends Declaration {
    public String name;
    public String path;

    public ImportSpec(boolean prefix, String idName, String stringPath) {
    	
    	this.path = stringPath;
    	if (prefix) {
    		if (!StringUtils.isBlank(idName)) {
		    	this.name = idName;
    		} else {
    			String[] tmp = stringPath.split("/");
    			this.name = tmp[tmp.length -1];
    		}
    	} else {
    		this.name = null;
    	}
    }

    @Override
    public Object accept(PasitoVisitor visitor) { return visitor.VisitDeclaration(this); }
}
