package pasito.ast.importSpec;

import pasito.ast.PasitoVisitor;

public abstract class Declaration {
	public abstract Object accept(PasitoVisitor visitor);
}
