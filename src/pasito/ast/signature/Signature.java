package pasito.ast.signature;

import java.util.List;

import pasito.ast.PasitoVisitor;


/**
 * Created by ariel on 20/08/17.
 */
public class Signature {
    public List<FormalParameter> outParameters;
    public FormalParameter variadicParameter; /* can be null */
    public List<FormalParameter> inParameters;

    public Signature(List<FormalParameter> inPars, FormalParameter variadicPar,
                     List<FormalParameter> outPars) {
        this.inParameters = inPars;
        this.variadicParameter = variadicPar;
        this.outParameters = outPars;
    }

    public Object accept(PasitoVisitor visitor) { return visitor.VisitSignature(this); }

}
