package pasito.ast.type;

import pasito.ast.PasitoVisitor;

/**
 * Created by ariel on 21/08/17.
 */
public class SliceType extends Type {
    public Type elemType;

    public SliceType(Type elementType) {
        this.elemType = elementType;
    }

    @Override
    public Object accept(PasitoVisitor visitor) { return visitor.VisitSliceType(this); }
}
