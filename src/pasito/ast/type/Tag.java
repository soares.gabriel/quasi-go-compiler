package pasito.ast.type;

import pasito.ast.PasitoVisitor;

/**
 * Created by Gabriel Soares on 08/03/18.
 */
public class Tag {
    public String name;
    
    public Tag(String name) {
        this.name = name;
    }

    public Object accept(PasitoVisitor visitor) { return visitor.VisitTag(this); }
}
