package pasito.ast.type;

import pasito.ast.PasitoVisitor;

public class PrimitiveType  extends Type  {

	public static final PrimitiveType INT32 = new PrimitiveType();
	public static final PrimitiveType FLOAT64 = new PrimitiveType();
	public static final PrimitiveType BOOLEAN = new PrimitiveType();
	
	private PrimitiveType() {}

    @Override
    public Object accept(PasitoVisitor visitor) { return visitor.VisitPrimitiveType(this); }
}
