package pasito.ast.type;

import pasito.ast.PasitoVisitor;
import pasito.ast.signature.Signature;

/**
 * Created by Gabriel Soares on 08/03/18.
 */
public class FunctionType extends Type{
    public Signature sig;

    public FunctionType(Signature sig) {
        this.sig = sig;
    }


    @Override
    public Object accept(PasitoVisitor visitor) {
        return visitor.VisitFunctionType(this);
    }
}
