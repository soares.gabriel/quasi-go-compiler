package pasito.ast.type;


import pasito.ast.PasitoVisitor;

/**
 * Created by Gabriel Soares on 05/03/18.
 */
public class MapType extends Type {
	public Type elemType;
	public Type keyType;
	
	public MapType(Type eType, Type kType) {
		this.elemType = eType;
		this.keyType = kType;
	}
	
	@Override
	public Object accept(PasitoVisitor visitor) {
		// TODO Auto-generated method stub
		return visitor.VisitMapType(this);
	}

}
