package pasito.ast.type;

import pasito.ast.PasitoVisitor;

/**
 * Created by Gabriel Soares on 08/03/18.
*/
public class TypeNameQualified extends Type{
    public String name1;
    public String name2;

    public TypeNameQualified(String n1, String n2) {
        this.name1 = n1;
        this.name2= n2;
    }

    @Override
    public Object accept(PasitoVisitor visitor) { return visitor.VisitTypeNameQualified(this); }
}
