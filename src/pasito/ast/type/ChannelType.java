package pasito.ast.type;


import pasito.ast.PasitoVisitor;

/*
 * Created by Gabriel Soares on 05/03/18.
 * Modified by Katharine Padilha on 09/03/2018.
 */
public class ChannelType extends Type {
	
	public Type elementType;
	public ChannelDirection direction;
	
	
	public ChannelType(Type elementType, ChannelDirection dir) {
		this.elementType = elementType;
		this.direction = dir;
	}

	@Override
	public Object accept(PasitoVisitor visitor) {
		// TODO Auto-generated method stub
		return visitor.VisitChannelType(this);
	}

}
