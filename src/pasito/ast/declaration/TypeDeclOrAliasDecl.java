package pasito.ast.declaration;

import pasito.ast.PasitoVisitor;

/**
 * Created by Gabriel Soares on 08/03/18.
*/
public abstract class TypeDeclOrAliasDecl extends Declaration {
	public abstract Object accept(PasitoVisitor visitor);
}
