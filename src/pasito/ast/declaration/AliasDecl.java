package pasito.ast.declaration;

import pasito.ast.PasitoVisitor;
import pasito.ast.type.Type;

/**
 * Created by Gabriel Soares on 08/03/18.
 */
public class AliasDecl extends TypeDeclOrAliasDecl {
    public Type type;
    public String name;

    public AliasDecl(String name, Type type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public Object accept(PasitoVisitor visitor) { return visitor.VisitAliasDecl(this); }
}
