package pasito.ast;

import java.util.List;

import pasito.ast.topLevelDecl.TopLevelDecl;
//import pasito.ast.importDecl.ImportDeclaration;
import pasito.ast.importSpec.ImportSpec;

/**
 * Created by Felipe Matheus on 05/03/18.
*/

public class SourceFile {
	public String identifier;
    public List<TopLevelDecl> topLevelDeclarations;
    public List<ImportSpec> importDeclarations;

    public SourceFile(String identifier, List<ImportSpec> importDeclarations, List<TopLevelDecl> topLevelDeclarations) {
        this.identifier = identifier;
    	this.topLevelDeclarations = topLevelDeclarations;
        this.importDeclarations = importDeclarations;
    }

    public Object accept(PasitoVisitor visitor) {
        return visitor.VisitSourceFile(this);
    }

}
