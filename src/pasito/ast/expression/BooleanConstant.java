package pasito.ast.expression;

import pasito.ast.ExpressionVisitor;

public class BooleanConstant extends Expression {
	public static final BooleanConstant TRUE = new BooleanConstant(true);
	public static final BooleanConstant FALSE = new BooleanConstant(false);
	
	public boolean value;

	private BooleanConstant(boolean value) {
		super();
		this.value = value;
	}

	
	@Override
    public Object accept(ExpressionVisitor visitor) { return visitor.VisitBooleanConstant(this); }
}
