package pasito.ast.expression;

import pasito.ast.ExpressionVisitor;

/**
 * Created by Gabriel Soares on 05/03/18.
 */
public class StringLiteral extends Expression {

	public String value;
	
	
	public StringLiteral(String values) {
		this.value = values;
	}

	@Override
	public Object accept(ExpressionVisitor visitor) {
		// TODO Auto-generated method stub
		return visitor.VisitStringLiteral(this); 
	}

}
