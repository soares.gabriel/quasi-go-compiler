package pasito.ast.expression;

import pasito.ast.ExpressionVisitor;

public class RuneLiteral extends Expression {
	public String value;

	public RuneLiteral(String value) {
		super();
		this.value = value;
	}

	@Override
	public Object accept(ExpressionVisitor visitor) {
		return visitor.VisitRuneLiteral(this);
	}	
}
