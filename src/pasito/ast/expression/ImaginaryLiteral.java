package pasito.ast.expression;

import pasito.ast.ExpressionVisitor;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexFormat;

public class ImaginaryLiteral extends Expression {
	public Complex value;
	
	public ImaginaryLiteral(Complex value) {
		super();
		this.value = value;
	}
	
	public ImaginaryLiteral(String value) {
		super();
		this.value = (new ComplexFormat()).parse(value);
	}

	@Override
	public Object accept(ExpressionVisitor visitor) {
		return visitor.VisitImaginaryLiteral(this);
	}

}
