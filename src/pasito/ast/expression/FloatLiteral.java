package pasito.ast.expression;

import pasito.ast.ExpressionVisitor;

/**
 * Created by ariel on 21/08/17.
 */
public class FloatLiteral extends Expression {

    public float value;

    public FloatLiteral(float value) {
        this.value = value;
    }

    @Override
    public Object accept(ExpressionVisitor visitor) { return visitor.VisitFloatLiteral(this); }
}
