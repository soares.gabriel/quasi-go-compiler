package pasito.ast.expression;

import pasito.ast.PasitoVisitor;

/**
 * Created by ariel on 21/08/17.
 * Modified by Gabriel Soares on 05/03/2018.
 */
public enum BinaryOperator {
    AND, OR, TIMES, DIV, PLUS, MINUS, LESS, EQ, NOTEQ, LEQ, GT, GEQ, BITOR, BITXOR, REMAINDER, LSHIFT, RSHIFT, BITAND, BITCLEAR;

    public Object accept(PasitoVisitor visitor) { return visitor.VisitBinaryOperator(this); }
}
