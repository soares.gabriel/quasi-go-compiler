package pasito.ast.expression;


/**
 * Created by ariel on 21/08/17.
 * Modified by Gabriel Soares on 05/03/2018.
 */
public enum UnaryOperator {
	
	/* unary_op   = "+" | "-" | "!" | "^" | "*" | "&" | "<-" . */
			
    PLUS, MINUS, NOT, BITXOR, POINTED_BY, BITAND, CHAN_OP;

//   public Object accept(ExpressionVisitor visitor) { return visitor.VisitUnaryOperator(this); }
}
