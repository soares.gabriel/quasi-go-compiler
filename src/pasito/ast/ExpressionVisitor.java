package pasito.ast;

import pasito.ast.expression.BinaryExpression;
import pasito.ast.expression.BooleanConstant;
import pasito.ast.expression.CallExpression;
import pasito.ast.expression.CompositeLit;
import pasito.ast.expression.FloatLiteral;
import pasito.ast.expression.FullSliceExpression;
import pasito.ast.expression.FunctionLiteral;
import pasito.ast.expression.IdExpression;
import pasito.ast.expression.ImaginaryLiteral;
import pasito.ast.expression.IndexExpression;
import pasito.ast.expression.IntLiteral;
import pasito.ast.expression.MethodExpression;
import pasito.ast.expression.RuneLiteral;
import pasito.ast.expression.SelectorExpression;
import pasito.ast.expression.SliceExpression;
import pasito.ast.expression.StringLiteral;
import pasito.ast.expression.UnaryExpression;

public interface ExpressionVisitor {

	Object VisitUnaryExpression(UnaryExpression unaryExpression);
    Object VisitBinaryExpression(BinaryExpression binaryExpression);
    Object VisitIntLiteral(IntLiteral intLiteral);
    Object VisitFloatLiteral(FloatLiteral floatLiteral);
    Object VisitFunctionLiteral(FunctionLiteral functionLiteral);
    Object VisitCompositLit(CompositeLit compositeLit);
    Object VisitIdExpression(IdExpression idExpression);
    Object VisitMethodExpression(MethodExpression methodExpression);
    Object VisitSelectorExpression(SelectorExpression selectorExpression);
    Object VisitIndexExpression(IndexExpression indexExpression);
    Object VisitSliceExpression(SliceExpression sliceExpression);
    Object VisitFullSliceExpression(FullSliceExpression fullSliceExpression);
    Object VisitCallExpression(CallExpression callExpression);
    Object VisitBooleanConstant(BooleanConstant booleanConstant);
    Object VisitStringLiteral(StringLiteral stringLiteral);
	Object VisitRuneLiteral(RuneLiteral runeLiteral);
	Object VisitImaginaryLiteral(ImaginaryLiteral imaginaryLiteral);
    
}