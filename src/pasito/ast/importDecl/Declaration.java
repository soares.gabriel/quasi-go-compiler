package pasito.ast.importDecl;

import pasito.ast.PasitoVisitor;

public abstract class Declaration {
	public abstract Object accept(PasitoVisitor visitor);
}
