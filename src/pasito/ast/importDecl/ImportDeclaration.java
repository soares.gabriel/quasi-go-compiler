package pasito.ast.importDecl;
import pasito.ast.PasitoVisitor;

public class ImportDeclaration extends Declaration {
    public Declaration decl;

    public ImportDeclaration(Declaration decl) {
        this.decl = decl;
    }

    @Override
    public Object accept(PasitoVisitor visitor) { return visitor.VisitDeclaration(this); }
}
