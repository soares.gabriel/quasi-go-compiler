package pasito.ast.element;

import pasito.ast.PasitoVisitor;
import pasito.ast.expression.Expression;

/**
 * Created by ariel on 21/08/17.
 */
//Classe alterada
public class KeyedElement {

    public Element elem;
    public Expression exp;

    public KeyedElement(Expression exp, Element elem) {
        this.elem = elem;
        this.exp = exp;
    }

    public Object accept(PasitoVisitor visitor) {
        return visitor.VisitKeyedElement(this);
    }
}
