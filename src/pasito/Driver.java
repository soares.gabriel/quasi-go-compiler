package pasito;

import pasito.ast.SourceFile;
import pasito.staticSemantics.Analyser;
import pasito.syntax.LexicalAnalyzer;
import pasito.syntax.Parser;
import pasito.ast.SourceFile;
import java_cup.runtime.*;
import java.io.FileReader;

/*
class Driver {

	public static void main(String[] args) throws Exception {
		Parser parser = new Parser();
		//parser.parse();
		parser.debug_parse();
	}
	
}
*/

class Driver {

	public static void main(String[] args) throws Exception {
		
		LexicalAnalyzer scanner = null;
		SourceFile sourceFile = null;
		
		try {
			ComplexSymbolFactory csf = new ComplexSymbolFactory();
			scanner = new LexicalAnalyzer(csf, new FileReader("tests/Teste.quasigo"));

			Parser parser = new Parser(scanner, csf);
			sourceFile = (SourceFile) parser.parse().value;
			
        	//PrettyPrint pp = new PrettyPrint(sourceFile);
        	//pp.VisitSourceFile(sourceFile);
      
        	Analyser an = new Analyser();
        	an.VisitSourceFile(sourceFile);
        	// System.out.println(" "); // Lucas
		}
		catch (java.io.FileNotFoundException e) {
			System.out.println("Input file not found");
		}
		catch (java.io.IOException e) {
			System.out.println("IO error scanning Input File ");
			System.out.println(e);
		}
		catch(Exception e) {
			System.out.println("Unexpected exception: ");
			e.printStackTrace();
		}    
	}

}
