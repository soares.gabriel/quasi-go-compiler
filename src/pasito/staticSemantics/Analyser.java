package pasito.staticSemantics;

import pasito.ast.PasitoVisitor;
import pasito.ast.SourceFile;
import pasito.ast.declaration.AliasDecl;
import pasito.ast.declaration.ConstDecl;
import pasito.ast.declaration.TypeDecl;
import pasito.ast.declaration.VarDecl;
import pasito.ast.element.ExpressionElement;
import pasito.ast.element.KeyedElement;
import pasito.ast.element.LiteralElement;
import pasito.ast.expression.BinaryExpression;
import pasito.ast.expression.BinaryOperator;
import pasito.ast.expression.BooleanConstant;
import pasito.ast.expression.CallExpression;
import pasito.ast.expression.CompositeLit;
import pasito.ast.expression.Expression;
import pasito.ast.expression.FloatLiteral;
import pasito.ast.expression.FullSliceExpression;
import pasito.ast.expression.FunctionLiteral;
import pasito.ast.expression.IdExpression;
import pasito.ast.expression.ImaginaryLiteral;
import pasito.ast.expression.IndexExpression;
import pasito.ast.expression.IntLiteral;
import pasito.ast.expression.MethodExpression;
import pasito.ast.expression.RuneLiteral;
import pasito.ast.expression.SelectorExpression;
import pasito.ast.expression.SliceExpression;
import pasito.ast.expression.StringLiteral;
import pasito.ast.expression.UnaryExpression;
import pasito.ast.expression.UnaryOperator;
import pasito.ast.expression.VariadicCommaOp;
import pasito.ast.importDecl.Declaration;
import pasito.ast.methodSpecOrInterfaceName.InterfaceName;
import pasito.ast.methodSpecOrInterfaceName.MethodSpec;
import pasito.ast.signature.FormalParameter;
import pasito.ast.signature.Signature;
import pasito.ast.statement.Assignment;
import pasito.ast.statement.Block;
import pasito.ast.statement.DeclarationStm;
import pasito.ast.statement.EmptyStmt;
import pasito.ast.statement.ExpressionStmt;
import pasito.ast.statement.ForRange;
import pasito.ast.statement.ForStmt;
import pasito.ast.statement.IfElseStmt;
import pasito.ast.statement.IfStmt;
import pasito.ast.statement.ReturnStmt;
import pasito.ast.statement.ShortVarDecl;
import pasito.ast.statement.Statement;
import pasito.ast.topLevelDecl.Dec;
import pasito.ast.topLevelDecl.FunctionDecl;
import pasito.ast.topLevelDecl.MethodDecl;
import pasito.ast.topLevelDecl.TopLevelDecl;
import pasito.ast.type.ArrayType;
import pasito.ast.type.ChannelType;
import pasito.ast.type.FieldDecl;
import pasito.ast.type.FunctionType;
import pasito.ast.type.InterfaceType;
import pasito.ast.type.MapType;
import pasito.ast.type.PointerType;
import pasito.ast.type.PrimitiveType;
import pasito.ast.type.SliceType;
import pasito.ast.type.StructType;
import pasito.ast.type.Tag;
import pasito.ast.type.TypeName;
import pasito.ast.type.TypeNameQualified;
import pasito.staticEnvironment.AlreadyBoundException;
import pasito.staticEnvironment.InvalidLevelException;
import pasito.staticEnvironment.SymbolTable;
import pasito.staticSemantics.binding.*;
import pasito.staticSemantics.type.NewType;
import pasito.staticSemantics.type.Nil;
import pasito.staticSemantics.type.Primitive;
import pasito.staticSemantics.type.Type;
import pasito.staticSemantics.type.Untyped;
import pasito.util.ErrorRegister;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/* TODO vists de exp e types devolvem tipos semanticos e usam tipos de ast somente */

/**
 * Modified by Gabriel Soares on 13/03/18.
 */
public class Analyser implements PasitoVisitor {

	SymbolTable<Binding> env;
	ConstantExpressionEavaluator constEvaluator;
	private List<Type> outTypes;

	public Analyser() {
		env = new SymbolTable<>();
		env.beginScope();
		try {
			
			/* 
			 * inicializar env com bindings predefinidos para int64, float64, boolean, ....
			 * The following identifiers are implicitly declared in the universe block:
			 */ 
			
			/* Types:
			 * 			bool byte complex64 complex128 error float32 float64
			 * 			int int8 int16 int32 int64 rune string
			 * 			uint uint8 uint16 uint32 uint64 uintptr
			 */

		    env.put("byte", new Ty(Primitive.BYTE));
		    env.put("complex64", new Ty(Primitive.COMPLEX64));
		    env.put("complex128", new Ty(Primitive.COMPLEX128));
		    env.put("error", new Ty(Primitive.ERROR));
		    env.put("rune", new Ty(Primitive.RUNE));
		    env.put("string", new Ty(Primitive.STRING));			
		    env.put("int", new Ty(Primitive.INT));
		    env.put("int8", new Ty(Primitive.INT8));
		    env.put("int16", new Ty(Primitive.INT16));
		    env.put("int32", new Ty(Primitive.INT32));
		    env.put("int64", new Ty(Primitive.INT64));
		    env.put("float32", new Ty(Primitive.FLOAT32));
		    env.put("float64", new Ty(Primitive.FLOAT64));		    
		    env.put("boolean", new Ty(Primitive.BOOLEAN));
		    env.put("uint", new Ty(Primitive.UINT));
		    env.put("uint8", new Ty(Primitive.UINT8));
		    env.put("uint16", new Ty(Primitive.UINT16));
		    env.put("uint32", new Ty(Primitive.UINT32));
		    env.put("uint64", new Ty(Primitive.UINT64));
		    env.put("uintptr", new Ty(Primitive.UINTPTR));
	    
			/* Constants:
			 * 			true false iota
			 */
		    env.put("true", new Const(true, Untyped.BOOLEAN_UNTYPED));
		    env.put("false", new Const(false, Untyped.BOOLEAN_UNTYPED));
		    env.put("iota", new Const(0, Untyped.INT_UNTYPED));
		    
			/* Zero value:
			 * 			nil
			 */
		    env.put("nil", new Const(null, Nil.getInstace()));	    
		    
		}
		catch(AlreadyBoundException e) {
			
		}
		constEvaluator = new ConstantExpressionEavaluator(env);
	}

	@Override
	public Object VisitSourceFile(SourceFile sourcefile) {
		for (TopLevelDecl tpDec : sourcefile.topLevelDeclarations)
			tpDec.accept(this);
		try {
			env.endScope();
		} catch (InvalidLevelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object VisitDec(Dec dec) {
		dec.decl.accept(this);
		return null;
	}

	@Override
	public Object VisitFunctionDecl(FunctionDecl functionDecl) {
		/*
		 * 1. cria o correspondente binding Fun e associar com o nome da função no
		 * ambiente env
		 * 2. env.beginScope()
		 * 3. elabora as declarações correspondentes aos parâmetros formais (i.e. visita a signature)
		 * 4. checa o corpo da função
		 * 5. env.endScope()
		 */
		Fun func;
		if(functionDecl.sig.variadicParameter != null)
		{
		    Type ty = (Type)functionDecl.sig.variadicParameter.accept(this);
		    func = new Fun(new ArrayList<Type>(), new ArrayList<Type>(), ty);
		}
		else 
		{
			func = new Fun(new ArrayList<Type>(), new ArrayList<Type>(), null);
		}
		try {
			env.put(functionDecl.name, func);
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(functionDecl.name + " já está declarada");
		}
		env.beginScope();
		Signature signature = functionDecl.sig;
		for (FormalParameter formalParam : signature.inParameters)
		{
			Type t = (Type) formalParam.accept(this);
			func.inTypes.add(t);
		}
		if(signature.outParameters != null)
		{
			for (FormalParameter formalParam : signature.outParameters)
			{
				Type t = (Type) formalParam.accept(this);
				func.outTypes.add(t);
			}
		}
		outTypes = func.outTypes;
		functionDecl.body.accept(this);
		try {
			env.endScope();
		} catch (InvalidLevelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public Object VisitMethodDecl(MethodDecl methodDecl) {
		// TODO Auto-generated method stub
		Meth method;
		if(methodDecl.sig.variadicParameter != null)
		{
			Type recTy = (Type)methodDecl.receiver.accept(this);
		    Type ty = (Type)methodDecl.sig.variadicParameter.accept(this);
		    method = new Meth(recTy, new ArrayList<Type>(), new ArrayList<Type>(), ty);
		}
		else 
		{
			Type recTy = (Type)methodDecl.receiver.accept(this);
			method = new Meth(recTy, new ArrayList<Type>(), new ArrayList<Type>(), null);
		}
		try {
			env.put(methodDecl.name, method);
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(methodDecl.name + " já está declarado");
		}
		
		if(methodDecl.receiver != null)
		{
			Type t = (Type) methodDecl.receiver.accept(this);
			method.receiverType = t;
		}
		
		env.beginScope();
		Signature signature = methodDecl.sig;
		for (FormalParameter formalParam : signature.inParameters)
		{
			Type t = (Type) formalParam.accept(this);
			method.inTypes.add(t);
		}
		if(signature.outParameters != null)
		{
			for (FormalParameter formalParam : signature.outParameters)
			{
				Type t = (Type) formalParam.accept(this);
				method.outTypes.add(t);
			}
		}
		outTypes = method.outTypes;
		methodDecl.body.accept(this);
		try {
			env.endScope();
		} catch (InvalidLevelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object VisitSignature(Signature signature) {
		if(signature.variadicParameter != null)
		{
		    Type ty = (Type)signature.variadicParameter.accept(this);
		}
		for (FormalParameter formalParam : signature.inParameters)
		{
			Type t = (Type) formalParam.accept(this);
			try {
				env.put(formalParam.name, new Var(t));
			} catch (AlreadyBoundException e) {
				ErrorRegister.report(formalParam.name + "já está declarado nesse escopo");
			}
		}
		if(signature.outParameters != null)
		{
			for (FormalParameter formalParam : signature.outParameters)
			{
				Type t = (Type) formalParam.accept(this);
				try {
					env.put(formalParam.name, new Var(t));
					outTypes.add(t);
				} catch (AlreadyBoundException e) {
					ErrorRegister.report(formalParam.name + "já está declarado nesse escopo");
				}
			}
		}
		return null;
	}

	@Override
	public Object VisitConstDecl(ConstDecl constDecl) {
		if (constDecl.exp == null) {
			ErrorRegister.report("error: ...");
			/* adicionar alguma entrada em env para recuperação de erros */
			return null;
		}

		Object value = constDecl.exp.accept(constEvaluator);
		Type ty = (Type) constDecl.exp.accept(this);
		if (constDecl.type == null)
			try {
				env.put(constDecl.name, new Const(value, ty));
			} catch (AlreadyBoundException e) {
				ErrorRegister.report("bla bla bla");
			}
		else {
			Type declaredType = (Type) constDecl.type.accept(this);
			try {
				env.put(constDecl.name, new Const(value, declaredType));
			} catch (AlreadyBoundException e) {
				ErrorRegister.report("bla bla bla");
			}
			//if (!ty.assignableTo(declaredType))
			if (Type.assignableTo(ty, declaredType))
				ErrorRegister.report("...");
		}
		return null;
	}

	@Override 
	public Object VisitVarDecl(VarDecl varDecl) {
		//Type ty = (Type) varDecl.exp.accept(this);
		Type ty = (Type) varDecl.type.accept(this);
		try {
			env.put(varDecl.name, new Var(ty));
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(varDecl.name + "já está declarado nesse escopo");
		}
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitTypeDecl(TypeDecl typeDecl) {
		Type ty = (Type) typeDecl.type.accept(this);
		try {
			Type t = null;
			if (ty instanceof NewType) {
				t = new NewType ((NewType) ty).underlyingType;
			} else {
				env.put(typeDecl.name, new Ty(ty));
			} 
			env.put(typeDecl.name, new Ty(t));
			
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(typeDecl.name + "já está declarado nesse escopo");
		}
		return null;
	}

	@Override
	public Object VisitFormalParameter(FormalParameter formalParameter) {
		Type ty = (Type) formalParameter.type.accept(this);
		try {
			env.put(formalParameter.name, new Var(ty));
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(formalParameter.name + "já está declarado nesse escopo");
		}
		return null;
	}

	@Override
	public Object VisitTypeName(TypeName typeName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitArrayType(ArrayType arrayType) {
		arrayType.elemType.accept(this);
		
//		if (arrayType.elemType instanceof ()Nil)
//			return Nil.getInstace();
		return null;
		
	}

	@Override
	public Object VisitBaseType(PointerType pointerType) {
		return new pasito.staticSemantics.type.PointerType((pasito.staticSemantics.type.Type) pointerType.baseType.accept(this));
	}

	@Override
	public Object VisitStructType(StructType structType) {
		for(FieldDecl field : structType.fieldDecls)
			field.accept(this);
		
		/*TODO checar campos com mesmo ID e montar lista*/
		return null;
	}

	@Override
	public Object VisitInterfaceType(InterfaceType interfaceType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitSliceType(SliceType sliceType) {
		sliceType.elemType.accept(this);
		return null;
	}

	@Override
	public Object VisitFieldDecl(FieldDecl fieldDecl) {
		Type ty = (Type) fieldDecl.type.accept(this);
		try {
			env.put(fieldDecl.name, new Ty(ty));
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(fieldDecl.name + "já está declarado nesse escopo");
		}
		return null;
	}

	@Override
	public Object VisitMethodSpec(MethodSpec methodSpec) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitInterfaceName(InterfaceName interfaceName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitUnaryExpression(UnaryExpression unaryExpression) {
		return unaryExpression.exp.accept(this);
	}

	@Override
	public Object VisitBinaryExpression(BinaryExpression binaryExpression) {
		Type tyl = (Type) binaryExpression.leftExp.accept(this);
		Type tyr = (Type) binaryExpression.rightExp.accept(this);
		
		//if (!tyl.assignableTo(tyr)) {
		if (!Type.assignableTo(tyl, tyr)) {
			ErrorRegister.report("Os dois tipos de um BinaryExpression devem ser equivalentes");
		}
		
//		if(tyl instanceof Nil || tyr instanceof Nil)
//			return Nil.getInstace();

		String op = binaryExpression.op.name();
		if(tyl == Primitive.BOOLEAN && (op == "EQ" || op == "NOTEQ")) {
			return Primitive.BOOLEAN;
		} else if ((tyl == Primitive.FLOAT || tyl == Primitive.INT ) 
			&& (op == "LT" || op == "GT" || op == "LESS" || op == "GEQ" || op == "LEQ")) {
			if (tyl == Primitive.FLOAT)
				return Primitive.FLOAT;
			else
				return Primitive.INT;
		}
		else
			return tyl;
	}

	@Override
	public Object VisitIntLiteral(IntLiteral intLiteral) {
		return Primitive.INT64;
	}

	@Override
	public Object VisitFloatLiteral(FloatLiteral floatLiteral) {
		return Primitive.FLOAT64;
	}

	@Override
	public Object VisitFunctionLiteral(FunctionLiteral functionLiteral) {
		
//		Fun func;
//		if(functionLiteral.sig.variadicParameter != null)
//		{
//		    Type ty = (Type)functionLiteral.sig.variadicParameter.accept(this);
//		    func = new Fun(new ArrayList<Type>(), new ArrayList<Type>(), ty);
//		}
//		else 
//		{
//			func = new Fun(new ArrayList<Type>(), new ArrayList<Type>(), null);
//		}
//		env.beginScope();
//		Signature signature = functionLiteral.sig;
//		for (FormalParameter formalParam : signature.inParameters)
//		{
//			Type t = (Type) formalParam.accept(this);
//			func.inTypes.add(t);
//		}
//		if(signature.outParameters != null)
//		{
//			for (FormalParameter formalParam : signature.outParameters)
//			{
//				Type t = (Type) formalParam.accept(this);
//				func.outTypes.add(t);
//			}
//		}
//		functionLiteral.body.accept(this);
//		try {
//			env.endScope();
//		} catch (InvalidLevelException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return null;

	}

	@Override
	public Object VisitCompositLit(CompositeLit compositeLit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitIdExpression(IdExpression idExpression) {
		Binding bnd = env.get(idExpression.name);
		if (bnd == null)
			ErrorRegister.report("A varivel " + idExpression.name + " não foi declarada");
		else if ( bnd instanceof Var )
			return ((Var) bnd).type;
		else if ( bnd instanceof Const )
			return ((Const) bnd).type;
		else 
			ErrorRegister.report(idExpression.name + " não é  uma varivel");
		return null;

	}

	@Override
	public Object VisitMethodExpression(MethodExpression methodExpression) {
		Type ty = (Type) methodExpression.type.accept(this);
		try {
			env.put(methodExpression.name, new Ty(ty));
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(methodExpression.name + "já está declarado nesse escopo");
		}
		return null;
	}

	@Override
	public Object VisitSelectorExpression(SelectorExpression selectorExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitIndexExpression(IndexExpression indexExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitSliceExpression(SliceExpression sliceExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitFullSliceExpression(FullSliceExpression fullSliceExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitCallExpression(CallExpression callExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitKeyedElement(KeyedElement keyedElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitExpressionElement(ExpressionElement expressionElement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitLiteralElement(LiteralElement literalElement) {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public Object VisitDeclarationStm(DeclarationStm declarationStm) {
		declarationStm.decl.accept(this);
		return null;
	}

	@Override
	public Object VisitEmptyStmt(EmptyStmt emptyStmt) {
		return null;
	}

	@Override
	public Object VisitReturnStmt(ReturnStmt returnStmt) {
		if (returnStmt.exps.size() != outTypes.size()) {
			ErrorRegister.report("Número de return statements é diferente do número de outputs");
		} else {
			Iterator <Type> it = outTypes.iterator(); 
			for (Expression exp : returnStmt.exps) {
				Type t = (Type) exp.accept(this);
				if (!t.assignableTo(t, it.next())) {
					ErrorRegister.report("Tipos incompatíveis na atribuição");
				}
			}
		}
		return null;
	}

	@Override
	public Object VisitExpressionStmt(ExpressionStmt expressionStmt) {
		expressionStmt.exp.accept(this);
		return null;
	}

	@Override
	public Object VisitAssignment(Assignment assignment) {
		// TODO Auto-generated method stub
		List<Expression> leftExps = assignment.leftExps;
		List<Expression> rightExps = assignment.rightExps;
		if (leftExps.size() != rightExps.size())
		{
		   ErrorRegister.report("Tamanhos na atribuição incompatíveis");
		}
		else
		{
			ListIterator<Expression> lIt = leftExps.listIterator();
			for(Expression rexp : rightExps)
			{
				Expression lexp = lIt.next();
				Type rty = (Type)rexp.accept(this);
				Type lty = (Type)lexp.accept(this);
				//if(!rty.assignableTo(lty))
				if(!Type.assignableTo(rty, lty))
				{
					ErrorRegister.report("Tipos incompatíveis na atribuição");
				}
			}
		}
		return null;
	}

	@Override
	public Object VisitShortVarDecl(ShortVarDecl shortVarDecl) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitBlock(Block block) {
		for (Statement st : block.stmts)
			st.accept(this);
		return null;
	}

	@Override
	public Object VisitIfStmt(IfStmt ifStmt) {
		if(ifStmt.initStmt != null)
			ifStmt.initStmt.accept(this);
		Type ty = (Type)ifStmt.exp.accept(this);
		if(ty instanceof Primitive)
		{
			Primitive p = (Primitive)ty;
			if(p.kind.name() != "BOOLEAN")
				ErrorRegister.report("Resultado da condição do IF deve ser booleano");
		}
		else
			ErrorRegister.report("Condição do IF deve ser uma expressão binária");
		ifStmt.block.accept(this);
		return null;
	}

	@Override
	public Object VisitIfElseStmt(IfElseStmt ifElseStmt) {
		if(ifElseStmt.initStmt != null)
			ifElseStmt.initStmt.accept(this);
		Type ty = (Type)ifElseStmt.exp.accept(this);
		if(ty instanceof Primitive)
		{
			Primitive p = (Primitive)ty;
			if(p.kind.name() != "BOOLEAN")
				ErrorRegister.report("Resultado da condição do IF deve ser booleano");
				
		}
		else
			ErrorRegister.report("Condição do IF deve ser uma expresso binária");
		ifElseStmt.leftBlock.accept(this);
		ifElseStmt.rightBlock.accept(this);
		return null;
	}

	@Override
	public Object VisitForStmt(ForStmt forStmt) {
		if(forStmt.initStmt != null)
			forStmt.initStmt.accept(this);
		Type ty = (Type)forStmt.exp.accept(this);
		if(ty instanceof Primitive)
		{
			Primitive p = (Primitive)ty;
			if(p.kind.name() != "BOOLEAN")
				ErrorRegister.report("Resultado da condição do FOR deve ser booleano");				
		}
		else
			ErrorRegister.report("Condição do FOR deve ser do tipo Primitive");
		forStmt.body.accept(this);
		if(forStmt.postStmt != null)
			forStmt.postStmt.accept(this);
		
		return null;
	}

	@Override
	public Object VisitForRange(ForRange forRange) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitBinaryOperator(BinaryOperator binaryOperator) {
		return binaryOperator;
	}

	@Override
	public Object VisitUnaryOperator(UnaryOperator unaryOperator) {
		return unaryOperator;
	}

	@Override
	public Object VisitPrimitiveType(PrimitiveType primitiveType) {
		return primitiveType;
	}

	@Override
	public Object VisitBooleanConstant(BooleanConstant booleanConstant) {
		return Primitive.BOOLEAN;
	}

	@Override
	public Object VisitVariadicCommaOp(VariadicCommaOp variadicCommaOp) {
		return variadicCommaOp;
	}

	@Override
	public Object VisitStringLiteral(StringLiteral stringLiteral) {
		return Primitive.STRING;
	}

	@Override
	public Object VisitDeclaration(Declaration declaration) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitMapType(MapType mapType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitChannelType(ChannelType channelType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitFunctionType(FunctionType functionType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitAliasDecl(AliasDecl aliasDecl) {
		Type ty = (Type) aliasDecl.type.accept(this);
		try {
			env.put(aliasDecl.name, new Ty(ty));
		} catch (AlreadyBoundException e) {
			ErrorRegister.report(aliasDecl.name + "já está declarado nesse escopo");
		}
		return null;
	}

	@Override
	public Object VisitTypeNameQualified(TypeNameQualified typeNameQualified) {
		typeNameQualified.accept(this);
		return null;
	}

	@Override
	public Object VisitTag(Tag tag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitRuneLiteral(RuneLiteral runeLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitImaginaryLiteral(ImaginaryLiteral imaginaryLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

}
