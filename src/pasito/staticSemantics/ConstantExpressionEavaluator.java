package pasito.staticSemantics;

import pasito.ast.ExpressionVisitor;
import pasito.ast.expression.BinaryExpression;
import pasito.ast.expression.BooleanConstant;
import pasito.ast.expression.CallExpression;
import pasito.ast.expression.CompositeLit;
import pasito.ast.expression.FloatLiteral;
import pasito.ast.expression.FullSliceExpression;
import pasito.ast.expression.FunctionLiteral;
import pasito.ast.expression.IdExpression;
import pasito.ast.expression.ImaginaryLiteral;
import pasito.ast.expression.IndexExpression;
import pasito.ast.expression.IntLiteral;
import pasito.ast.expression.MethodExpression;
import pasito.ast.expression.RuneLiteral;
import pasito.ast.expression.SelectorExpression;
import pasito.ast.expression.SliceExpression;
import pasito.ast.expression.StringLiteral;
import pasito.ast.expression.UnaryExpression;
import pasito.ast.type.ChannelType;
import pasito.staticEnvironment.SymbolTable;
import pasito.staticSemantics.binding.Binding;
import pasito.util.ErrorRegister;

/* The object returned by the evaluation is an Integer, Float or Boolean,
 * depending on the type of the constant
*/
public class ConstantExpressionEavaluator implements ExpressionVisitor {

	SymbolTable<Binding> env;
	
	public ConstantExpressionEavaluator(SymbolTable<Binding> env) {
		this.env = env;
	}

	@Override
	public Object VisitUnaryExpression(UnaryExpression unaryExpression) {
		Object value = unaryExpression.exp.accept(this);
		if (value != null)
			switch (unaryExpression.op) {
			case PLUS: 
				if (value instanceof Integer || value instanceof Float)
					return value;
				else {
					ErrorRegister.report("Not numeric operand");
					return null;
				}
			case MINUS:
				if (value instanceof Integer)
					return - (Integer) value;
				else if (value instanceof Float)
					return - (Float) value;
				else {
					ErrorRegister.report("Not numeric operand");
					return null;
				}
			case NOT:
				if (value instanceof Boolean)
					return ! ((Boolean) value).booleanValue();
				else {
					ErrorRegister.report("Not boolean operand");
					return null;
				}
			case POINTED_BY:

			case BITXOR:

			case BITAND:

			case CHAN_OP:

			default:
				ErrorRegister.report("Cannot point to a constant value");
				return null;
			}
		else
			return null;
	}

	@Override
	public Object VisitBinaryExpression(BinaryExpression binaryExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitIntLiteral(IntLiteral intLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitFloatLiteral(FloatLiteral floatLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitFunctionLiteral(FunctionLiteral functionLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitCompositLit(CompositeLit compositeLit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitIdExpression(IdExpression idExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitMethodExpression(MethodExpression methodExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitSelectorExpression(SelectorExpression selectorExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitIndexExpression(IndexExpression indexExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitSliceExpression(SliceExpression sliceExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitFullSliceExpression(FullSliceExpression fullSliceExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitCallExpression(CallExpression callExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitBooleanConstant(BooleanConstant booleanConstant) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitStringLiteral(StringLiteral stringLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitRuneLiteral(RuneLiteral runeLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object VisitImaginaryLiteral(ImaginaryLiteral imaginaryLiteral) {
		// TODO Auto-generated method stub
		return null;
	}

}

