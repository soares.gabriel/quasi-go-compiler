package pasito.staticSemantics.type;

import java.util.List;

import pasito.ast.methodSpecOrInterfaceName.MethodSpecOrInterfaceName;
/**
 * Created by Giovanny on 14/09/17.
 */
public class InterfaceType extends Type {
    public List<MethodSpecOrInterfaceName> interfaceElems;

    public InterfaceType(List<MethodSpecOrInterfaceName> interfaceElems) {
        this.interfaceElems = interfaceElems;
    }

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two interface types are identical if they have the same set of 
		 * methods with the same names and identical function types. 
		 * Non-exported method names from different packages are always 
		 * different. The order of the methods is irrelevant. */
		
		return false;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}
}
