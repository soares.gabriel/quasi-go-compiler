package pasito.staticSemantics.type;

/**
 * Modified by Gabriel Soares on 13/03/18.
 */
public class Primitive extends Type {
	public Kind kind;

	private Primitive(Kind kind) {
		this.kind = kind;
	}
	
	public static Type BYTE = new NewType(new Primitive(Kind.BYTE));
	
	public static Type COMPLEX64 = new NewType(new Primitive(Kind.COMPLEX));
	public static Type COMPLEX128 = new NewType(new Primitive(Kind.COMPLEX));

	public static Type ERROR = new NewType(new Primitive(Kind.INT));

	public static Type FLOAT = new NewType(new Primitive(Kind.FLOAT));
	public static Type FLOAT32 = new NewType(new Primitive(Kind.FLOAT));
	public static Type FLOAT64 = new NewType(new Primitive(Kind.FLOAT));

	public static Type INT = new NewType(new Primitive(Kind.INT));
	public static Type INT8 = new NewType(new Primitive(Kind.INT));
	public static Type INT16 = new NewType(new Primitive(Kind.INT));
	public static Type INT32 = new NewType(new Primitive(Kind.INT));
	public static Type INT64 = new NewType(new Primitive(Kind.INT));

	public static Type RUNE = new NewType(new Primitive(Kind.RUNE));
	public static Type STRING = new NewType(new Primitive(Kind.STRING));

	public static Type UINT = new NewType(new Primitive(Kind.UINT));
	public static Type UINT8 = new NewType(new Primitive(Kind.UINT));
	public static Type UINT16 = new NewType(new Primitive(Kind.UINT));
	public static Type UINT32 = new NewType(new Primitive(Kind.UINT));
	public static Type UINT64 = new NewType(new Primitive(Kind.UINT));
	public static Type UINTPTR = new NewType(new Primitive(Kind.UINT));
	
	public static Type BOOLEAN = new NewType(new Primitive(Kind.BOOLEAN));

	@Override
	public boolean equivalent(Type ty) {
		return ty == this;
	}
	

//	@Override
//	public boolean assignableTo(Type ty) {
//		if(ty instanceof Primitive)
//		{
//			if(((Primitive) ty).kind.name() == kind.name())
//				return true;
//			else
//                return false;
//		}
//		else
//			return false;
//	}
}
