package pasito.staticSemantics.type;

import java.util.List;

import pasito.ast.PasitoVisitor;


/**
 * Created Felipe Matheus and Giovanny on 13/03/18.
 */
public class Signature {
    public List<Type> outParameters;
    public Type variadicParameter; /* can be null */
    public List<Type> inParameters;

    public Signature(List<Type> outPars, Type variadicPar, List<Type> inPars) {
		super();
		this.outParameters = outPars;
		this.variadicParameter = variadicPar;
		this.inParameters = inPars;
	}

}
