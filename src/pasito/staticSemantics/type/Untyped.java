package pasito.staticSemantics.type;

public class Untyped extends Type {
	public Kind kind;
	public static Untyped INT_UNTYPED = new Untyped(Kind.INT);
	public static Untyped FLOAT_UNTYPED = new Untyped(Kind.FLOAT);
	public static Untyped BOOLEAN_UNTYPED = new Untyped(Kind.BOOLEAN);

	public Untyped(Kind kind) {
		super();
		this.kind = kind;
	}

	@Override
	public boolean equivalent(Type ty) {
		return this == ty;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}
