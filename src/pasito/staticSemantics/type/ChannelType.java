package pasito.staticSemantics.type;

/**
 * Created by Gabriel Soares on 12/03/18.
 * Modified by Felipe Matheus and Katharine Padilha on 12/03/18
 */
public class ChannelType extends Type{
	public Type elementType;
	public ChannelDirection direction;
	
	
	public ChannelType(Type elementType, ChannelDirection dir) {
		this.elementType = elementType;
		this.direction = dir;
	}

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two channel types are identical if they have identical element 
		 * types and the same direction. 
		 */
		
		if (ty instanceof ChannelType) {
			
			if ( this.direction == ((ChannelType) ty).direction 
					&& this.elementType.equivalent(((ChannelType) ty).elementType) ) {
				return true;
			}
		}
		
		return false;
	}
	
//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}	

}
