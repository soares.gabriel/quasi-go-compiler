package pasito.staticSemantics.type;

/**
 * Created by Giovanny on 14/09/17.
 * Modified by Felipe Matheus on 12/03/18.
 */
public class NewType extends Type{
    public Type underlyingType;

    public NewType(Type type) {
        this.underlyingType = type;
    }

	@Override
	public boolean equivalent(Type ty) {
		return this == ty;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}
}
