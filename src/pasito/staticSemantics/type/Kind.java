package pasito.staticSemantics.type;

/**
 * Modified by Gabriel Soares on 13/03/18.
 */
public enum Kind {
	INT, FLOAT, BOOLEAN, BYTE, ERROR, RUNE, STRING, UINT, UINTPTR, COMPLEX;
}
