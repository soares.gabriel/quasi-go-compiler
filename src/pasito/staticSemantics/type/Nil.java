package pasito.staticSemantics.type;

public final class Nil extends Type{
	private static final Nil INSTANCE = new Nil();

	private Nil() {
		
	}
	
	public static Nil getInstace() {
		return INSTANCE;
	}

	@Override
	public boolean equivalent(Type ty) {
		return this == ty;
	}
}
