package pasito.staticSemantics.type;

/**
 * Created by Giovanny on 21/09/17.
 * Modified by Gabriel Soares on 12/03/18.
 * Modified by Felipe Matheus and Katharine Padilha on 12/03/18.
 */
public class PointerType extends Type {
    public Type baseType;

    public PointerType(Type baseType) {
        this.baseType = baseType;
    }

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two pointer types are identical if they have identical base types. */
		
		if (ty instanceof PointerType) {
			
			return this.baseType.equivalent(((PointerType) ty).baseType);
		}
		return false;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}
