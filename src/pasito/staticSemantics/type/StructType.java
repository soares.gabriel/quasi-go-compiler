package pasito.staticSemantics.type;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by Giovanny on 14/09/17.
 * Modified by Gabriel Soares on 12/03/18.
 * Modified by Felipe Matheus and Giovanny on 13/03/18. 
 */
public class StructType extends Type {

    List<Pair<String, Type>> fieldDecls;

    public StructType(List<Pair<String, Type>> fieldDecls) {
        this.fieldDecls = fieldDecls;
    }

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two struct types are identical if they have the same sequence 
		 * of fields, and if corresponding fields have the same names, and 
		 * identical types, and identical tags. Non-exported field names 
		 * from different packages are always different. 
		 */
		if (ty instanceof StructType) {
			if (this.fieldDecls.size() == ((StructType) ty).fieldDecls.size()) {
				Iterator<Pair<String, Type>> it = ((StructType) ty).fieldDecls.iterator();
				for (Pair<String, Type> p : fieldDecls) {
				
					Pair<String, Type> p1 = it.next();
					if(!p1.getLeft().equals(p.getLeft()) || !p1.getRight().equivalent(p.getRight()))
						return false;
						
					}
				
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}
