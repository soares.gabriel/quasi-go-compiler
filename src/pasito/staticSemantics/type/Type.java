package pasito.staticSemantics.type;

/**
 * Created by Giovanny on 14/09/17.
 */

/* Underlying Types */

public abstract class Type {
	/**************
	 *  A implementao deste mtodo deve seguir a definio de type identity dada em
	 *   https://golang.org/ref/spec#Type_identity
	 */
	public abstract boolean equivalent(Type ty);

	
	/**********
	 * A implementao deste mtodo deve seguir a definio de assignability dada em
	 * https://golang.org/ref/spec#Assignability
	 * 
	 * A value x is assignable to a variable of type T ("x is assignable to T") if one 
	 * of the following conditions applies:
	 * 
	 * 	-->	x's type is identical to T.
	 * 	-->	x's type V and T have identical underlying types and at least one of V or T is not a defined type. 
	 *	--> [IGNORED] T is an interface type and x implements T. 
	 * 	-->	x is a bidirectional channel value, T is a channel type, x's type V and T have identical element 
	 *   		types, and at least one of V or T is not a defined type.
	 * 	-->	x is the predeclared identifier nil and T is a pointer, function, slice, map, channel, or interface type.
	 * 	-->	x is an untyped constant representable by a value of type T.
	 */

	public final static boolean assignableTo(Type ty1, Type ty2) {
		return ty1.equivalent(ty2) || 
				(!ty1.defined() || !ty2.defined()) && ty1.underlying().equivalent(ty2.underlying()) ||
				assignableChannel(ty1, ty2) ||
				ty1 == Nil.getInstace() && ((ty2 instanceof PointerType) || (ty2 instanceof FunctionType) || (ty2 instanceof SliceType)) ||
				ty1 instanceof Untyped && compatible((Untyped)ty1, ty2);
	}


	private static boolean assignableChannel(Type ty1, Type ty2) {
		return ty1 instanceof ChannelType 
				&& ((ChannelType) ty1).direction == ChannelDirection.BIDIRECTIONAL 
				&& ty2 instanceof ChannelType
				&& ((ChannelType) ty1).elementType.equivalent(((ChannelType) ty2).elementType)
				&& (!ty1.defined() || !ty2.defined());
	}


	private static boolean compatible(Untyped ty1, Type ty2) {
		return ty2 instanceof Untyped && ((Untyped) ty2).kind == ty1.kind ||
				ty2 instanceof Primitive && ((Primitive) ty2).kind == ty1.kind ||
				ty2 instanceof NewType && ((NewType) ty2).underlyingType instanceof Primitive && ((Primitive) ((NewType) ty2).underlyingType).kind == ty1.kind;
	}


	private Type underlying() {
		if(this instanceof NewType)
			return ((NewType) this).underlyingType;
		else
			return this;
	}


	private boolean defined() {
		return this instanceof NewType;
	};

}
