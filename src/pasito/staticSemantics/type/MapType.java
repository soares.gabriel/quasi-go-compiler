package pasito.staticSemantics.type;

/**
 * Created by Gabriel Soares on 05/03/18.
 * Modified by Felipe Matheus and Katharine Padilha on 12/03/18
 */
public class MapType extends Type {
	public Type elemType;
	public Type keyType;
	
	public MapType(Type eType, Type kType) {
		this.elemType = eType;
		this.keyType = kType;
	}

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two map types are identical if they have identical key and element types. */
		if (ty instanceof MapType) {
			
			return (this.elemType.equivalent(((MapType) ty).elemType) 
					&& this.keyType.equivalent(((MapType) ty).keyType));
		}		

		return false;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}
		


}
