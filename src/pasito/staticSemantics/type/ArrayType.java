package pasito.staticSemantics.type;


/**
 * Created by Giovanny on 14/09/17.
 * Modified by Gabriel Soares on 12/03/18.
 */
public class ArrayType extends Type {
    public Type elemType;
    public int length;

    public ArrayType (int length, Type elemType) {
        this.length = length;
        this.elemType = elemType;
    }

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two array types are identical if they have identical element types and the same array length. */
		
		if (ty instanceof ArrayType) {
			return (this.length ==  ((ArrayType) ty).length 
					&& this.elemType.equivalent(((ArrayType) ty).elemType));
		}
		
		return false;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}


}
