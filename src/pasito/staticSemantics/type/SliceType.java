package pasito.staticSemantics.type;

/**
 * Created by Giovanny on 14/09/17.
 * Modified by Gabriel Soares on 12/03/18.
 */
public class SliceType extends Type {
    public Type elementType;

    public SliceType(Type elementType) {
        this.elementType = elementType;
    }

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two slice types are identical if they have identical element types. */
		
		if (ty instanceof SliceType) {
			return this.elementType.equivalent(((SliceType) ty).elementType);
		}
		
		return false;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}
