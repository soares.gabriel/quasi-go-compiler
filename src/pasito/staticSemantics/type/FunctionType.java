package pasito.staticSemantics.type;

import java.util.Iterator;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by Gabriel Soares on 12/03/18.
 * Modified by Felipe Matheus and Katharine Padilha on 12/03/18.
 */
public class FunctionType extends Type {
    public Signature sig;

    public FunctionType(Signature sig) {
        this.sig = sig;
    }

	@Override
	public boolean equivalent(Type ty) {
		
		/* Two function types are identical if they have the same number 
		 * of parameters and result values, corresponding parameter and 
		 * result types are identical, and either both functions are 
		 * variadic or neither is. Parameter and result names are not 
		 * required to match. */

		if (ty instanceof FunctionType) {
			if(this.sig.inParameters.size() == ((FunctionType) ty).sig.inParameters.size() 
					&& (this.sig.variadicParameter != null || ((FunctionType) ty).sig.variadicParameter == null)) {
				Iterator<Type> it = ((FunctionType) ty).sig.inParameters.iterator();
				for (Type t : sig.inParameters) {
				
					Type t1 = it.next();
					if(!t1.equivalent(t))
						return false;
						
					}
			}
			if(this.sig.outParameters.size() == ((FunctionType) ty).sig.outParameters.size()) {
				Iterator<Type> it = ((FunctionType) ty).sig.outParameters.iterator();
				for (Type t : sig.outParameters) {
				
					Type t1 = it.next();
					if(!t1.equivalent(t))
						return false;
						
					}
			} else 				
				return true;
		}
		return false;
	}

//	@Override
//	public boolean assignableTo(Type ty) {
//		// TODO Auto-generated method stub
//		return false;
//	}


}
