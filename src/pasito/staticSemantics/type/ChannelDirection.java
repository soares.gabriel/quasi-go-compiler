package pasito.staticSemantics.type;

/**
 * Created by Gabriel Soares on 13/03/18.
 */
public enum ChannelDirection {
	/*
	 * 	BIDIRECTIONAL - chan T      // can be used to send and receive values of type T
	 *	SENDER - chan<- float64  	// can only be used to send float64s
	 *	RECEIVER - <-chan int      	// can only be used to receive ints
	 */
	BIDIRECTIONAL, RECEIVER, SENDER;
}
