package CompiladorPasito;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;

import pasito.syntax.LexicalAnalyzer;
import java_cup.runtime.ComplexSymbolFactory;
import pasito.PrettyPrint;
import pasito.ast.SourceFile;
import pasito.staticSemantics.Analyser;
import pasito.syntax.Parser;
import pasito.syntax.sym;


public class Main {
	public static void main(String[] args) {
        
		String caminho = Paths.get("").toAbsolutePath().toString();
        String codigoFonte = caminho + "/src/Teste.Pasito";
         
        ComplexSymbolFactory f = new ComplexSymbolFactory(); // cria instância do ComplexSymbolFactory

        File file = new File(codigoFonte); // lê arquivo de entrada em Pasito
        
        FileInputStream fis = null; 
        
        try {
            fis = new FileInputStream(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        LexicalAnalyzer lexical = new LexicalAnalyzer(f, fis); // Instancia o Analisador Léxico

        try {
			while (lexical.next_token().sym != sym.EOF) { // Enquanto houverem novos tokens...
			    //System.out.println("\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	Parser parser = new Parser(new LexicalAnalyzer(f, new FileReader(codigoFonte)), f);
        	SourceFile p = (SourceFile) parser.parse().value;
        	
        	PrettyPrint pp = new PrettyPrint(p);
        	pp.VisitSourceFile(p);
      
        	Analyser an = new Analyser();
        	an.VisitSourceFile(p);
        	//parser.debug_parse(); //SourceFile p = (SourceFile) parse.parse();
        	                      //Analyzer an = new Analyzer();
        	                      //p.accept(an);
        } catch(Exception e) {
        	e.printStackTrace();
        }
    
	}
}
