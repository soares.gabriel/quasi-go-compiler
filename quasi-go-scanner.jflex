package pasito.syntax;

import java_cup.runtime.ComplexSymbolFactory.Location;
import java_cup.runtime.ComplexSymbolFactory;
import java.io.IOException;
import java_cup.runtime.Symbol;
import java.io.FileInputStream;
import org.apache.commons.math3.complex.ComplexFormat;

%%

%debug

// nome da classe a ser gerada pelo jflex
%class LexicalAnalyzer

// privacidade da scanner gerado
%public

%unicode

// habilita contagem de linhas (yyline -- começa na linha 0)
%line
// habilita contagem de caracteres (yychar -- começa na coluna 0)
%char
// habilita contagem de colunas (yycolumn -- começa em 0)
%column

/*
* yyline: linha analisada no momento atual pelo analisador lÃ©xico (lembre do automato) (comeÃ§a na linha 0)
* yycolumn: coluna analisada no momento atual pelo analisador lÃ©xico (lembre do automato) (comeÃ§a na coluna 0)
* yychar: caractere analisado no momento atual pelo analisador lÃ©xico (lembre do automato)
* yylenght(): retorna comprimento da palavra de entrada no momento atual do analisador lÃ©xico (lembre do automato)
* yytext(): palavra de entrada no momento atual do analisador lÃ©xico (lembre do automato)
*/

%cup

// implementa a classe sym gerada pelo cup
%implements sym

%{
	private ComplexSymbolFactory symbolFactory;
    private Symbol ultimoSimbolo;
    private StringBuffer string = new StringBuffer();

    /*
    * Construtor utilizado na chamada do analisador lÃ©xico pelo CUP
    * @param sf instÃ¢ncia da fÃ¡brica de sÃ­mbolos implementada pelo CUP que usaremos
    * @param is instÃ¢ncia do arquivo de entrada em Pasito que serÃ¡ analisado
    */
    public LexicalAnalyzer(ComplexSymbolFactory sf, FileInputStream is) {
        this(new java.io.InputStreamReader(is));
        symbolFactory = sf;
    }
    public LexicalAnalyzer(ComplexSymbolFactory sf, java.io.Reader reader) {
        this(reader);
        symbolFactory = sf;
    }

    /*
    * Método fábricas de símbolos
    * O sémbolo retornado é uma instância de ComplexSymbolFactory, implementada pelo CUP
    */

    public Symbol symbol(String nome, int code) {

    	System.out.println("<" + nome + ", " + yytext() + "> (" + (yyline + 1) + " - " + (yycolumn + 1) + ")");

    	ultimoSimbolo = symbolFactory.newSymbol(nome, code,
                            new Location(yyline+1, yycolumn+1, yychar),
                            new Location(yyline+1, yycolumn+yylength(), yychar+yylength()));
        return ultimoSimbolo;
    }

    public Symbol symbol(String nome, int code, Object value) {
    	
		System.out.println("<" + nome + ", " + value + "> (" + (yyline + 1) + " - " + (yycolumn + 1) + ")");

    	ultimoSimbolo = symbolFactory.newSymbol(nome, code,
                            new Location(yyline+1, yycolumn+1, yychar),
                            new Location(yyline+1, yycolumn+yylength(), yychar+yylength()),
                            value);
        return ultimoSimbolo;
    }

%}


%eofval{
	//System.exit(0); //Armengo para evitar loop infinito
  return symbol("EOF",EOF);
%eofval}

// início das definiçõees regulares


/* Comentários */
Comentario_bloco = "/*" ~"*/"
Comentario_linha = "//" .*
Comentario = {Comentario_bloco} | {Comentario_linha}

/* Delimitadores */
Fim_linha = \r\n | [\r\n\u2028\u2029\u000B\u000C\u0085] // fim de linha
Espaco_Branco = [\t\f ] // espaço em branco
espacos = {Espaco_Branco}* | {Comentario} // tudo isso será ignorado

caracter_unicode = [^\n]
letra_unicode = [:letter:] // Unicode letters
digito_unicode = [:digit:] // Unicode digits
digito_dec = [0-9]
digito_oct = [0-7]
digito_hex = [0-9] | [A-F] | [a-f]

/* Identificadores*/
Id = {Letra} ( {Letra} | {Digito} )* //identifier
Letra = {letra_unicode} | "_"
Digito = {digito_unicode}

/* Literais Numéricos */
Numero_int = {Numero_dec} | {Numero_oct} | {Numero_hex}
Numero_dec = [1-9] {digito_dec}*
Numero_oct = "0" {digito_oct}*
Numero_hex = "0" ( "x" | "X" ) {digito_hex} {digito_hex}*

Decimal = {digito_dec} {digito_dec}*
Expoente = ("e" | "E") ("+" | "-")? {Decimal}
Numero_float = {Decimal} "." {Decimal}? {Expoente}? | {Decimal} {Expoente} | "." {Decimal} {Expoente}?

/* Literais Imaginários */
Numero_imag = ({Decimal} | {Numero_float}) "i"

/* Literais Rune */
Rune_lit = "'" ( {valor_unicode} | {valor_byte} ) "'"
valor_byte       = {valor_byte_octal} | {valor_byte_hex}
valor_byte_octal = "\\" {digito_oct} {digito_oct} {digito_oct}
valor_byte_hex   = "\\" "x" {digito_hex} {digito_hex}
valor_unicode    = {caracter_unicode} | {valor_u_peq} | {valor_u_gran} | {caracter_escapado}
valor_u_peq   = "\\" "u" {digito_hex} {digito_hex} {digito_hex} {digito_hex}
valor_u_gran = "\\" "U" {digito_hex} {digito_hex} {digito_hex} {digito_hex}
                          {digito_hex} {digito_hex} {digito_hex} {digito_hex}
caracter_escapado = "\\" ( "a" | "b" | "f" | "n" | "r" | "t" | "v" | "\\" | "'" | "\"" )
                          

/* Estados */
%state INTERPRETED_STRING

%%

// Tokens com suas ações correspondentes
<YYINITIAL> {
	{Fim_linha}    	{ if( ultimoSimbolo != null )
							if (	ultimoSimbolo.sym == ID ||
						   			ultimoSimbolo.sym == INT ||
									ultimoSimbolo.sym == INT8 ||
						   			ultimoSimbolo.sym == INT16 ||
						   			ultimoSimbolo.sym == INT32 ||
									ultimoSimbolo.sym == INT64 ||
									ultimoSimbolo.sym == FLOAT ||
									ultimoSimbolo.sym == FLOAT32 ||
									ultimoSimbolo.sym == FLOAT64 ||
									ultimoSimbolo.sym == COMPLEX ||
									ultimoSimbolo.sym == COMPLEX64 ||
									ultimoSimbolo.sym == COMPLEX128 ||
									ultimoSimbolo.sym == RUNE_LIT ||
									ultimoSimbolo.sym == STRING_LIT ||
									ultimoSimbolo.sym == BREAK ||
									ultimoSimbolo.sym == CONTINUE ||
									ultimoSimbolo.sym == FALLTHROUGH ||
									ultimoSimbolo.sym == RETURN ||
									ultimoSimbolo.sym == INC_OP ||
									ultimoSimbolo.sym == DEC_OP ||
									ultimoSimbolo.sym == RPAR ||
									ultimoSimbolo.sym == RSBRACK ||
									ultimoSimbolo.sym == RBRACK 
								)
					  return symbol("SEMICOLON", SEMICOLON); 
					}

	{espacos}		{ }

	/* Palavras reservadas */ // *** Estes tokens deveriam ser tratados como identificadores
	bool  			{ return symbol("BOOLEAN", BOOLEAN); }
	int32  			{ return symbol("INT32", INT32); }
	float64  		{ return symbol("FLOAT64", FLOAT64); }
	
	break      		{ return symbol("BREAK", BREAK);}
	case  			{ return symbol("CASE", CASE); }
	CHAN      		{ return symbol("CHAN", CHAN);}
	const  	  		{ return symbol("CONST", CONST); }
	continue 	 	{ return symbol("CONTINUE", CONTINUE);}
	default   		{ return symbol("DEFAULT", DEFAULT); }
	defer     		{ return symbol("DEFER", DEFER);}
	else  			{ return symbol("ELSE", ELSE); }
	fallthrough 	{ return symbol("FALLTHROUGH", FALLTHROUGH); }
	for  			{ return symbol("FOR", FOR); }
	func  			{ return symbol("FUNC", FUNC); }
	go      		{ return symbol("GO", GO);}
	goto      		{ return symbol("GOTO", GOTO);}
	if 				{ return symbol("IF", IF); }
	import    		{ return symbol("IMPORT", IMPORT);}
	interface 		{ return symbol("INTERFACE", INTERFACE); }
	map      		{ return symbol("MAP", MAP);}
	package   		{ return symbol("PACKAGE", PACKAGE);}
	range     		{ return symbol("RANGE", RANGE);}
	return  		{ return symbol("RETURN", RETURN); }
	select    		{ return symbol("SELECT", SELECT);}
	struct 			{ return symbol("STRUCT", STRUCT); }
	switch  		{ return symbol("SWITCH", SWITCH); }
	type  			{ return symbol("TYPE", TYPE); }
	var  			{ return symbol("VAR", VAR); }

	{Numero_int}	{ return symbol("INT", INT, Integer.parseInt(yytext())); }
	{Numero_float}  { return symbol("FLOAT", FLOAT, Float.parseFloat(yytext())); }
	{Numero_imag} 	{ return symbol("IMAGINARY", IMAGINARY, (new ComplexFormat()).parse(yytext())); }

	/* Operadores */
	"+"				{ return symbol("PLUS", PLUS); }
	"-"				{ return symbol("MINUS", MINUS); }
	"*"				{ return symbol("TIMES", TIMES); }
	"/"				{ return symbol("DIV", DIV); }
	"%"				{ return symbol("REMAINDER", REMAINDER); }
	"&"				{ return symbol("BITAND", BITAND); }
	"|"				{ return symbol("BITOR", BITOR); }
	"^"				{ return symbol("BITXOR", BITXOR); }
	"<<"			{ return symbol("LSHIFT", LSHIFT); }
	">>"			{ return symbol("RSHIFT", RSHIFT); }
	"&^"			{ return symbol("BITCLEAR", BITCLEAR); }

	"+="			{ return symbol("PLUS_ASSIGN", PLUS_ASSIGN); }
	"-="			{ return symbol("MINUS_ASSIGN", MINUS_ASSIGN); }
	"*="			{ return symbol("TIMES_ASSIGN", TIMES_ASSIGN); }
	"/="			{ return symbol("DIV_ASSIGN", DIV_ASSIGN); }
	"%="			{ return symbol("REMAINDER_ASSIGN", REMAINDER_ASSIGN); }
	"&="			{ return symbol("BITAND_ASSIGN", BITAND_ASSIGN); }
	"|="			{ return symbol("BITOR_ASSIGN", BITOR_ASSIGN); }
	"^="			{ return symbol("BITXOR_ASSIGN", BITXOR_ASSIGN); }
	"&^="			{ return symbol("BITCLEAR_ASSIGN", BITCLEAR_ASSIGN); }
	"<<="			{ return symbol("LSHIFT_ASSIGN", LSHIFT_ASSIGN); }
	">>=" 			{ return symbol("RSHIFT_ASSIGN", RSHIFT_ASSIGN); }
	
	"&&" 			{ return symbol("AND", AND); }
	"||" 			{ return symbol("OR", OR); }
	"<-"			{ return symbol("CHAN_OP", CHAN_OP); }

	"++" 			{return  symbol("INC_OP", INC_OP); }	// PLUSPLUS
	"--" 			{return  symbol("DEC_OP", DEC_OP); }	// MINUSMINUS

	"=="			{ return symbol("EQ", EQ); }
	"<"				{ return symbol("LT", LT); }
	">"				{ return symbol("GT", GT); }
	"="				{ return symbol("ASSIGN", ASSIGN); }
	"!"				{ return symbol("NOT", NOT); }

	"!="			{ return symbol("NOTEQ", NOTEQ); }
	"<=" 			{ return symbol("LEQ", LEQ); }
	">=" 			{ return symbol("GEQ", GEQ); }
	":="			{ return symbol("DASSIGN", DASSIGN); }
	"..."			{ return symbol("DOTDOTDOT", DOTDOTDOT); }
	"("				{ return symbol("LPAR", LPAR); }
	"["				{ return symbol("LSBRACK", LSBRACK); }
	"{"				{ return symbol("LBRACK", LBRACK); }
	","				{ return symbol("COMMA", COMMA); }
	"."		    	{ return symbol("DOT", DOT);}

	")"				{ return symbol("RPAR", RPAR); }
	"]"				{ return symbol("RSBRACK", RSBRACK); }
	"}"				{ return symbol("RBRACK", RBRACK); }
	";"				{ return symbol("SEMICOLON", SEMICOLON); }
	":"				{ return symbol("COLON", COLON); }

	/* Literais */
	\"              { string.setLength(0); yybegin(INTERPRETED_STRING); }
	
	\` ~\` 			{ /* Tratamento de Raw Strings */
		              string.append( yytext() );	
					  return symbol("RAW_STRING", STRING_LIT, string.toString().replace("`", "")); 
					}
	
	{Rune_lit} 		{ return symbol("RUNE", RUNE_LIT); }

	/* Identificadores */
	{Id}			{ return symbol("ID", ID, yytext()); }
	
	/* Tratamento de caracteres não reconhecidos */
	.				{ System.out.println(
						"Scanner warning >> " +
						"Unrecognized character '" + yytext() + "' -- ignored" + " at line" +
                		(yyline+1) + ", column " + (yycolumn+1) ); 
                	}
}

/* Tratamento de Interpreted Strings */
<INTERPRETED_STRING> {
    \"                             { yybegin(YYINITIAL); return symbol("INTERPRETED_STRING", STRING_LIT, string.toString()); }
    [^\n\r\"\\]+                   { string.append( yytext() ); }
    \\t                            { string.append('\t'); }
    \\n                            { string.append('\n'); }
    \\r                            { string.append('\r'); }
    \\\"                           { string.append('\"'); }
    \\                             { string.append('\\'); }
    \\x {digito_hex}{2}			   {int code = Integer.parseInt((yytext().substring(2,4)), 16); string.append(Character.toChars(code));}
    \\ {digito_oct}{3}			   {int code = Integer.parseInt((yytext().substring(1,4)), 8); string.append(Character.toChars(code));}
    \\u {digito_hex}{4}			   {int code = Integer.parseInt((yytext().substring(2,6)), 16); string.append(Character.toChars(code));}
    \\U {digito_hex}{8}			   {int code = Integer.parseInt((yytext().substring(2,10)), 16); string.append(Character.toChars(code));}
  }